# PUBLIC-SUBNET-for Bastion servers

  resource "aws_subnet" "subnet" {
    vpc_id                 = vpc.aws_vpc.three-tier-vpc
    cidr_block             = vpc.aws_vpc.vpc_cidr_block
    availability_zone     = var.availability_zones
    map_public_ip_on_launch = var.map_public_ip_on_launch

    tags = {
      Name = var.Name
    }
  }